<?php

$dsn = 'mysql:host=localhost;dbname=db_login';
$username = 'root';
$password = '';
$options = [];
try {
    $connection = new PDO($dsn, $username, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Database connection fail" . $e->getMessage();
}
